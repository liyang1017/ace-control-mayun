import { Component, OnInit,Input,Output,EventEmitter,TemplateRef } from '@angular/core';

@Component({
  selector: 'ace-switch',
  templateUrl: './ace-switch.component.html',
  styleUrls: ['./ace-switch.component.less']
})
export class AceSwitchComponent implements OnInit {

  @Input() aceSize:String = 'm';
  @Input() aceDisabled:Boolean = false;
  @Input() aceModel:boolean = false;
  @Input() aceCheckedChildren: string | TemplateRef<void>;
  @Input() aceUnCheckedChildren: string | TemplateRef<void>;
  @Output() aceModelChange = new EventEmitter();
  constructor() { }

  ngOnInit() {
  }

  onclick(){
    if(!this.aceDisabled){
      this.aceModel = !this.aceModel;
      this.aceModelChange.emit(this.aceModel);
    }
  }
}
