import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { AceSwitchComponent } from './ace-switch.component';

describe('AceSwitchComponent', () => {
  let component: AceSwitchComponent;
  let fixture: ComponentFixture<AceSwitchComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ AceSwitchComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(AceSwitchComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
