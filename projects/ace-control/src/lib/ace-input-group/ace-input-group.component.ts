import { Component, OnInit, Input, TemplateRef,ElementRef, ContentChild, Output, EventEmitter  } from '@angular/core';
import { AceInputDirective } from './../ace-input/ace-input.directive'

@Component({
  selector: 'ace-input-group',
  templateUrl: './ace-input-group.component.html',
  styleUrls: ['./ace-input-group.component.less']
})
export class AceInputGroupComponent implements OnInit {

  constructor(private el:ElementRef) { }

  @Input() aceIcon:String = "";
  @Input() aceIconAfter:String = "";
  @Input() aceTemplate:TemplateRef<any>;
  @Input() aceTemplateAfter:TemplateRef<any>;
  @Input() aceBlock:Boolean = false;
  @Input() aceType:String = 'input';
  @Input() aceLength:Number = 0;

  @Output() afterClick = new EventEmitter();

  @ContentChild(AceInputDirective,{static:false}) private input:any;

  iconSizeClass = "ace-input-group-default";

  textareaLength:Number = 0;

  ngOnInit() {
  }

  ngAfterContentInit(){
    let th = this;
    if(th.aceType == 'input'){
      if(th.input.aceSize == 'l'){
        th.iconSizeClass = "ace-input-group-larger";
        if(th.aceIconAfter || th.aceTemplateAfter){
          th.input.el.nativeElement.classList.add('ace-input-group-input-larger-after');
        }
        if(th.aceIcon || th.aceTemplate){
          th.input.el.nativeElement.classList.add('ace-input-group-input-larger');
        }
      }else{
        th.iconSizeClass = "ace-input-group-default";
        if(th.aceIconAfter || th.aceTemplateAfter){
          th.input.el.nativeElement.classList.add('ace-input-group-input-default-after'); 
        }
        if(th.aceIcon || th.aceTemplate){
          th.input.el.nativeElement.classList.add('ace-input-group-input-default');
        }
      }
      if(th.input.aceBlock !== false){
        th.aceBlock = true;
      }else{
        th.aceBlock = false;
      }
    }else if(th.aceType == 'textarea'){
      if(th.input.aceBlock !== false){
        th.aceBlock = true;
      }else{
        th.aceBlock = false;
      }
      th.input.el.nativeElement.onkeyup = function (event) {
      }
    }
    
  }

  afterFun(){
    let th = this;
    th.afterClick.emit('');
  }

  mouseover(){
    let th = this;
    th.input.aceHover = true;
    th.input.el.nativeElement.classList.add('ace-input-hover');
  }
  mouseout(){
    let th = this;
    th.input.aceHover = false;
    this.input.el.nativeElement.classList.remove('ace-input-hover');
  }
}
