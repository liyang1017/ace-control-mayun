import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { AceInputGroupComponent } from './ace-input-group.component';

describe('AceInputGroupComponent', () => {
  let component: AceInputGroupComponent;
  let fixture: ComponentFixture<AceInputGroupComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ AceInputGroupComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(AceInputGroupComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
