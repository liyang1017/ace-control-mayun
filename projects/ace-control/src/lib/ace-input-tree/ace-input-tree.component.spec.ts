import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { AceInputTreeComponent } from './ace-input-tree.component';

describe('AceInputTreeComponent', () => {
  let component: AceInputTreeComponent;
  let fixture: ComponentFixture<AceInputTreeComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ AceInputTreeComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(AceInputTreeComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
