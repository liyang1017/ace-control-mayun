import { Component, OnInit, Input,Output, EventEmitter, ViewChild, ElementRef } from '@angular/core';
import { AceTreeComponent } from './../ace-tree/ace-tree.component';

@Component({
  selector: 'ace-input-tree',
  templateUrl: './ace-input-tree.component.html',
  styleUrls: ['./ace-input-tree.component.less']
})
export class AceInputTreeComponent implements OnInit {

  constructor() { }

  @Input() aceSize:string = 'm';

  @Input() aceSearch:string = '';

  @Input() disabled:boolean = false;

  @Input() acePlaceholder:string = '请选择';

  @Input() aceDropStyle:object = {};

  @Input() aceInputStyle:object = {};

  @Input() aceAllowClear:boolean = false;

  @Input() aceLoading:boolean = true;

  @Input() aceSelect:any = {};

  @Input() aceTreeData:Array<any> = [];

  @Input() aceKeyName:string = '';

  @Input() aceOnSelect:Function;

  @Input() aceShowLength:Number = 1000;

  @Input() aceShowDeep:number = 2;

  @Output() aceSearchChange = new EventEmitter();

  @Output() aceSelectChange = new EventEmitter();

  @Output() aceComplate = new EventEmitter();

  searchTimeout:any = '';

  placeholderCopy:string = '';

  isfocus:boolean = false;

  treeComplate:boolean = false;

  visible:boolean = false;

  seachCopy:string = '';

  aceFocusNode:any = {};

  isMouse:boolean = false;

  @ViewChild('tree',{static:false}) public tree:AceTreeComponent;

  @ViewChild('input',{static:false}) input:ElementRef;

  uid:string = '';

  ngOnInit() {
    this.placeholderCopy = this.acePlaceholder;
    this.uid = this.guid();
  }

  valueChange(value){
    let th = this;
    th.aceLoading = true;
    if(th.treeComplate){
      if(th.searchTimeout){
        clearTimeout(th.searchTimeout);
        th.searchTimeout = "";
      }
      th.searchTimeout = setTimeout(() => {
        th.tree.aceQueryByName(value);
        th.aceLoading = false;
      }, 500);
    }else{
      setTimeout(() => {
        th.valueChange(value);
      }, 200);
    }
  }

  keyChange(event){
    let th = this;
    if(event.keyCode == '40'){
      th.tree.keyChange('down') 
    }else if(event.keyCode == '38'){
      th.tree.keyChange('up');
    }else if(event.keyCode == '13'){
      th.enterConfirm();
    }
  }

  enterConfirm(){
    let th = this;
    if(th.aceOnSelect){
      let aceReturn  = th.aceOnSelect(th.aceFocusNode);
      if(!aceReturn)return;
    }
    th.aceSelectChangeFun(th.aceFocusNode)
  }

  focusInput(){
    let th = this;
    th.isMouse = false;
    setTimeout(() => {
      th.isfocus = true;
    }, 100);
    th.visible = true;
    if(th.aceSelect && th.aceSelect.id){
      th.placeholderCopy = th.aceSearch;
      th.aceFocusNode = JSON.parse(JSON.stringify(th.aceSelect));
    }else{
      th.placeholderCopy = th.acePlaceholder;
    }
    th.aceSearch = th.seachCopy;
  }

  blurInput(a){
    if(!a.sourceCapabilities)return;
    let th = this;
    th.aceFocusNode = {};
    th.isfocus = false;
    th.visible = false;
    th.seachCopy = th.aceSearch;
    if(th.aceSelect && th.aceSelect.id){
      th.aceSearch = th.aceSelect.parentNameArry && th.aceSelect.parentNameArry.length > 0 ? th.aceSelect.parentNameArry.join('/') + '/' + th.aceSelect[th.aceKeyName] : th.aceSelect[th.aceKeyName];
    }else{
      th.aceSearch = '';
      th.placeholderCopy = th.acePlaceholder;
    }
  }

  onmousedownClose(e){
    if(e && e.preventDefault ){
      e.preventDefault();
    }
  }

  expandOrclose(){
    let th = this;
    if(th.visible){
      th.input.nativeElement.blur('abc');
    }else{
      th.input.nativeElement.focus();
    }
  }

  aceSelectChangeFun(value){
    let th = this;
    th.seachCopy = th.aceSearch;
    th.aceSelect = value;
    th.aceSearch = th.aceSelect.parentNameArry && th.aceSelect.parentNameArry.length > 0 ? th.aceSelect.parentNameArry.join('/') + '/' + th.aceSelect[th.aceKeyName] : th.aceSelect[th.aceKeyName];
    th.aceSelectChange.emit(value);
    th.input.nativeElement.blur();
    th.visible = false;
    th.isfocus = false;
    th.aceFocusNode = {};
  }

  aceComplateFun(){
    this.treeComplate = true;
    this.aceComplate.emit();
  }

  aceSelectTreeNode(id){
    let th = this;
    if(th.treeComplate){
      let treeNode = th.tree.aceGetTreeNodeById(id);
      th.aceSelect = treeNode;
      if(th.aceSelect){
        th.aceSearch = th.aceSelect.parentNameArry && th.aceSelect.parentNameArry.length > 0 ? th.aceSelect.parentNameArry.join('/') + '/' + th.aceSelect[th.aceKeyName] : th.aceSelect[th.aceKeyName];
        th.aceSelectChange.emit(th.aceSelect);
      }
    }else{
      setTimeout(() => {
        th.aceSelectTreeNode(id)
      }, 200);
    }
  }

  refreshTree(){
    let th = this;
    th.seachCopy = '';
    th.aceSearch = '';
    th.placeholderCopy = th.acePlaceholder;
    th.aceSelect = {};
    th.aceSelectChange.emit(th.aceSelect);
    th.tree.aceRetract(true);
  }

  S4() {
    return (((1 + Math.random()) * 0x10000) | 0).toString(16).substring(1);
  }
  guid() {
    let th = this;
    return (th.S4() + th.S4() + "-" + th.S4() + "-" + th.S4() + "-" + th.S4() + "-" + th.S4() + th.S4() + th.S4());
  }

}
