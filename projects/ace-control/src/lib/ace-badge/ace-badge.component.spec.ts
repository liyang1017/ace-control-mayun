import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { AceBadgeComponent } from './ace-badge.component';

describe('AceBadgeComponent', () => {
  let component: AceBadgeComponent;
  let fixture: ComponentFixture<AceBadgeComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ AceBadgeComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(AceBadgeComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
