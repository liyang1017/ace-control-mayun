import { Component, OnInit, Input, TemplateRef, SimpleChanges, ViewChild, ElementRef } from '@angular/core';

@Component({
  selector: 'ace-badge',
  templateUrl: './ace-badge.component.html',
  styleUrls: ['./ace-badge.component.less']
})
export class AceBadgeComponent implements OnInit {

  constructor() { }

  notWrapper = true;
  viewInit = false;
  maxNumberArray: string[] = [];
  countArray: number[] = [];
  countSingleArray = [0, 1, 2, 3, 4, 5, 6, 7, 8, 9];
  colorArray = [
    'pink',
    'red',
    'yellow',
    'orange',
    'cyan',
    'green',
    'blue',
    'purple',
    'geekblue',
    'magenta',
    'volcano',
    'gold',
    'lime'
  ];
  presetColor: string | null = null;
  count: number;

  @ViewChild('contentElement', { static: false }) contentElement: ElementRef;
  @Input() aceColor: string;
  @Input() aceStyle: object = {};
  @Input() aceCount: number;
  @Input() aceDot = false;
  @Input() aceOverflowCount: number = 99;
  @Input() aceShowZero: boolean = false;

  ngOnChanges(changes: SimpleChanges): void {
    const { aceOverflowCount, aceCount, aceColor } = changes;
    if (aceCount && !(aceCount.currentValue instanceof TemplateRef)) {
      this.count = Math.max(0, aceCount.currentValue);
      this.countArray = this.count
        .toString()
        .split('')
        .map(item => +item);
    }
    if (aceOverflowCount) {
      this.generateMaxNumberArray();
    }
  }

  get showSup(): boolean {
    return (this.aceDot) || this.count > 0 || (this.count === 0 && this.aceShowZero);
  }


  ngOnInit() {
    this.generateMaxNumberArray();
  }

  generateMaxNumberArray(): void {
    this.maxNumberArray = this.aceOverflowCount.toString().split('');
  }

}
