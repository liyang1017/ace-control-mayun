import { Component, OnInit, Input, ViewChild, TemplateRef, Output, EventEmitter, ChangeDetectorRef,
OnDestroy, OnChanges, SimpleChanges } from '@angular/core';
import { _isNumberValue } from '@angular/cdk/coercion';
import { Subject } from 'rxjs';

export interface PaginationItemRenderContext {
  $implicit: 'page' | 'prev' | 'next';
  page: number;
}

@Component({
  // tslint:disable-next-line:component-selector
  selector: 'ace-pagination',
  templateUrl: './ace-pagination.component.html',
  styleUrls: ['./ace-pagination.component.less']
})
export class AcePaginationComponent implements OnInit, OnDestroy, OnChanges {
  firstPage = 1;
  pages: number[] = [];
  private $destroy = new Subject<void>();
  @Input() aceItemRender: TemplateRef<PaginationItemRenderContext>;
  @ViewChild('renderItemTemplate', { static: true }) aceItemRenderChild: TemplateRef<PaginationItemRenderContext>;
  get itemRender(): TemplateRef<PaginationItemRenderContext> {
    return this.aceItemRender || this.aceItemRenderChild;
  }

  @Output() readonly acePageIndexChange: EventEmitter<number> = new EventEmitter();
  @Output() readonly acePageSizeChange: EventEmitter<number> = new EventEmitter();

  @Input() aceShowTotal: TemplateRef<{ $implicit: number; range: [number, number] }>;
  @Input() aceInTable = false;
  @Input() aceSize: 'default' | 'small' | 'small-nb' = 'default';
  @Input() acePaginations = [10, 20, 30, 40];
  @Input() aceDisabled = false;
  @Input() aceShowSizeChanger = false;
  @Input() aceHideOnSinglePage = false;
  @Input() aceShowQuickJumper = false;
  @Input() aceSimple = false;
  @Input() aceTotal = 0;
  @Input() acePageIndex = 1;
  @Input() acePageSize = 10;

  get lastPage(): number {
    if (!this.acePageSize) {
      return 0;
    }

    return Math.ceil(this.aceTotal / this.acePageSize);
  }

  get isLastPage(): boolean {
    return this.lastPage === this.acePageIndex;
  }

  get isFirstPage(): boolean {
    return this.acePageIndex === this.firstPage;
  }

  get rangePages(): number[] {
    return [(this.acePageIndex - 1) * this.acePageSize + 1, Math.min(this.acePageIndex * this.acePageSize, this.aceTotal)];
  }

  prevPage(): void {
    this.gotoPage(this.acePageIndex - 1);
  }

  nextPage(): void {
    this.gotoPage(this.acePageIndex + 1);
  }

  prevSeveralPages(nums: number): void {
    this.gotoPage(this.acePageIndex - nums);
  }

  nextSevralPages(nums: number): void {
    this.gotoPage(this.acePageIndex + nums);
  }

  gotoPage(index: number): void {
    if (index !== this.acePageIndex && !this.aceDisabled) {
      const pageIndex = this.validatePageIndex(index);
      if (pageIndex !== this.acePageIndex) {
        this.updatePageIndex(pageIndex);
      }
    }
  }

  updatePageIndex(page: number): void {
    this.acePageIndex = page;
    this.acePageIndexChange.emit(this.acePageIndex);
    this.generateIndexes();
  }

  handleKeyDown(_: KeyboardEvent, input: HTMLInputElement, clearInputValue: boolean) {
    const target = input;
    const page = this.toNumber(target.value, this.acePageIndex);
    if (this.isInteger(page) && this.isPageIndexValid(page) && page !== this.acePageIndex) {
      this.updatePageIndex(page);
    }
    if (clearInputValue) {
      target.value = '';
    } else {
      target.value = `${this.acePageIndex}`;
    }
  }

  /** generate indexes list */
  generateIndexes(): void {
    const pages: number[] = [];
    if (this.lastPage <= 9) {
      for (let i = 2; i <= this.lastPage - 1; i++) {
        pages.push(i);
      }
    } else {
      const current = +this.acePageIndex;
      let left = Math.max(2, current - 2);
      let right = Math.min(this.lastPage - 1, current + 2);
      if (current - 1 <= 2) {
        right = 5;
      }
      if (this.lastPage - current <= 2) {
        left = this.lastPage - 4;
      }
      for (let i = left; i <= right; i++) {
        pages.push(i);
      }
    }

    this.pages = pages;
    this.cdr.markForCheck();
  }

  /** pageIndex validator  */
  validatePageIndex(value: number): number {
    if (value > this.lastPage) {
      return this.lastPage;
    } else if (value < this.firstPage) {
      return this.firstPage;
    } else {
      return value;
    }
  }

  isPageIndexValid(value: number): boolean {
    return this.validatePageIndex(value) === value;
  }

  /** data convert function */
  isInteger(value: string | number): boolean {
    return typeof value === 'number' && isFinite(value) && Math.floor(value) === value;
  }

  toNumber(value: number | string, fallbackValue: number = 0): number {
    return _isNumberValue(value) ? Number(value) : fallbackValue;
  }

  constructor(private cdr: ChangeDetectorRef) { }

  ngOnInit(): void {
  }

  ngOnDestroy(): void {
    this.$destroy.next();
    this.$destroy.complete();
  }

  ngOnChanges(changes: SimpleChanges): void {
    if (changes.aceTotal || changes.acePageSize || changes.acePageIndex) {
      this.generateIndexes();
    }
  }

}
