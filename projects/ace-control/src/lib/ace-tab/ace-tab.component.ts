import { Component, OnInit, Input, Output, EventEmitter, ElementRef,TemplateRef } from '@angular/core';

@Component({
  selector: 'ace-tab',
  templateUrl: './ace-tab.component.html',
  styleUrls: ['./ace-tab.component.less']
})
export class AceTabComponent implements OnInit {

  constructor(private el:ElementRef) { }

  @Input() aceIcon:String = '';
  @Input() aceTitle:String = '';
  @Input() disabled:any = false;
  @Input() aceSelectKey:number = 0;
  @Input() aceTemplate:TemplateRef<any>;
  @Output() aceSelectKeyChange = new EventEmitter();

  tabWidth:String = 'auto';
  classTab:any = "slideInLeft";
  key:number = 0;
  aceLength:number = 0;
  

  ngOnInit() {
  }

  selectFun(){
    let th = this;
    if(th.disabled || th.key === th.aceSelectKey)return;
    th.aceSelectKeyChange.emit(th.key);
  }


}
