import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { AceTabComponent } from './ace-tab.component';

describe('AceTabComponent', () => {
  let component: AceTabComponent;
  let fixture: ComponentFixture<AceTabComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ AceTabComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(AceTabComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
