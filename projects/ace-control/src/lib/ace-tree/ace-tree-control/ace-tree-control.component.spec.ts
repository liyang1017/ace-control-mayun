import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { AceTreeControlComponent } from './ace-tree-control.component';

describe('AceTreeControlComponent', () => {
  let component: AceTreeControlComponent;
  let fixture: ComponentFixture<AceTreeControlComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ AceTreeControlComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(AceTreeControlComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
