import { Component, OnInit, Input,Output, EventEmitter } from '@angular/core';

@Component({
  selector: 'ace-tree-control',
  templateUrl: './ace-tree-control.component.html',
  styleUrls: ['./ace-tree-control.component.less']
})
export class AceTreeControlComponent implements OnInit {

  constructor() { }

  @Input() aceTreeData: any = [];
  @Input() aceKeyName: any = 'name';
  @Input() aceSelect: any = {};
  @Input() aceParent: any = {};
  @Input() aceFocusNode:any = {};
  @Input() parent:any = {};
  @Input() aceOnSelect:Function;
  @Input() aceMulti:Boolean = false;
  @Input() aceUid:Boolean = false;
  

  @Output() aceSelectChange = new EventEmitter();
  @Output() aceMultiSelectChange = new EventEmitter();

  ngOnInit() {
  }

  selectChange(tree){
    let th = this;
    if(th.aceMulti)return;
    if(th.aceOnSelect){
      let aceReturn  = th.aceOnSelect(tree);
      if(!aceReturn)return;
    }
    th.aceSelect = tree;
    th.aceSelectChange.emit(tree);
  }

  aceSelectChanged(tree){
    let th = this;
    th.aceSelect = tree;
    th.aceSelectChange.emit(tree);
  }

  //多选树勾选更新状态
  updateAllChecked(treeDataOne) {
    let th = this;
    var updateAllCheckedFun = function name(treeNode) {
      treeNode.halfChecked = false;
      if (treeNode.checked) {
        if (treeNode.children && treeNode.children.length > 0) {
          treeNode.children.forEach(item => {
            if(!item.disable){
              item.checked = true;
              updateAllCheckedFun(item);
            }
          });
        }
      } else {
        if (treeNode.children && treeNode.children.length > 0) {
          treeNode.children.forEach(item => {
            if(!item.disable){
              item.checked = false;
              updateAllCheckedFun(item);
            }
          });
        }
      }
    }

    updateAllCheckedFun(treeDataOne)

    th.updateParentChecked();
  }

  updateParentChecked() {
    let th = this;
    if (th.parent && th.parent.children) {
      if (th.parent.children.every(item => !item.checked && !item.halfChecked)) {
        th.parent.checked = false;
        th.parent.halfChecked = false;

      } else if (th.parent.children.every(item => item.checked && !item.halfChecked)) {
        th.parent.checked = true;
        th.parent.halfChecked = false;
      } else {
        th.parent.halfChecked = true;
        th.parent.checked = true;
      }
    }
    th.aceMultiSelectChange.emit();
  }

  multiChangeFun(){
    this.updateParentChecked();
  }
}
