import { Component, OnInit, ViewChild, ElementRef, Input, SimpleChange, Output, EventEmitter } from '@angular/core';

@Component({
  selector: 'ace-tree',
  templateUrl: './ace-tree.component.html',
  styleUrls: ['./ace-tree.component.less']
})
export class AceTreeComponent implements OnInit {

  constructor() { }

  @ViewChild('search',{static:false}) searchHtml:ElementRef;

  contentHeight:String = "calc(100% - 32px)";

  @Input() aceSearch:Boolean = true;

  @Input() aceMulti:Boolean = false;

  @Input() aceLoading:Boolean = true;

  @Input() aceNodata:Boolean = false;

  @Input() aceTreeData:Array<any> = [];

  @Input() aceSelect:any = {};

  @Input() aceKeyName:string = "name";

  @Input() aceOnSelect:Function;

  @Input() aceShowLength:Number = 1000;

  @Input() aceShowDeep:number = 2;

  @Output() aceSelectChange = new EventEmitter();

  @Output() aceComplate = new EventEmitter();

  aceDataArry:Array<any> = [];

  searchKey:string = "";

  searchTimeout:any = "";

  aceDateKey:Array<any> = [];

  @Input() aceFocusNode:any = {};
  @Input() aceUid:string = "";
  
  @Output() aceFocusNodeChange = new EventEmitter();
  

  ngOnInit() {
    this.setDataArry(this.aceTreeData);
  }

  ngAfterViewInit(){
    setTimeout(() => {
      if(this.aceSearch){
        if(this.searchHtml.nativeElement.clientHeight){
          this.contentHeight = "calc(100% - " + this.searchHtml.nativeElement.clientHeight + "px)";
        }else{
          this.contentHeight = '100%';
        }
      }else{
        this.contentHeight = '100%';
      }
    }, 500);
  }

  setDataArry(data) {
    let th = this;
    var getDataArry = function (treeNode, parent,deep) {
      let parentIdArry: any = [];
      let parentNameArry:any = [];
      if (parent) {
        parentIdArry = JSON.stringify(parent.parentIdArry);
        parentIdArry = JSON.parse(parentIdArry);
        parentIdArry.push(parent.id);
        parentNameArry = JSON.stringify(parent.parentNameArry);
        parentNameArry = JSON.parse(parentNameArry);
        parentNameArry.push(parent[th.aceKeyName]);
      }
      if (treeNode && treeNode.length > 0) {
        for(let index = 0;index < treeNode.length;index ++){
          let item  = treeNode[index];
          if(parent){
            item.parentId = parent.id;
          }else{
            item.parentId = '';
          }
          item.indexKey = index;
          item.deep = deep;
          if(deep < (th.aceShowDeep - 1)){
            item.open = true;
          }
          item.parentIdArry = parentIdArry;
          item.parentNameArry = parentNameArry;
          th.aceDataArry.push(item);
          th.aceDateKey[item.id] = item;
          if (item.children && item.children.length > 0) {
            getDataArry(item.children, item, deep + 1);
          }
        }
      }
    }
    if(!data || data.length === 0){
      th.aceNodata = true;
    }else{
      th.aceNodata = false;
    }
    getDataArry(data, '', 0);
    th.aceComplate.emit();
  }

  //根据输入内容查询
  aceQueryByName(value){
    let th = this;
    if(th.aceDataArry && th.aceDataArry.length > 0){
      if(value != ''){
        let parentId = [];
        let showNode = [];
        let indexOfDataId = [];
        for(var a = th.aceDataArry.length - 1;a >= 0; a --){
          if(parentId.indexOf(th.aceDataArry[a].id) > -1){
            if(th.aceDataArry[a][th.aceKeyName] && th.aceDataArry[a][th.aceKeyName].indexOf(value) > -1){
              indexOfDataId.push(th.aceDataArry[a].id);
            }
            th.aceDataArry[a].notShow = false;
            showNode.push(th.aceDataArry[a]);
            parentId = parentId.concat(th.aceDataArry[a].parentIdArry);
          }else{
            if(th.aceDataArry[a][th.aceKeyName] && th.aceDataArry[a][th.aceKeyName].indexOf(value) > -1){
              indexOfDataId.push(th.aceDataArry[a].id);
              th.aceDataArry[a].notShow = false;
              showNode.push(th.aceDataArry[a]);
              th.aceDataArry[a].open = true;
              parentId = parentId.concat(th.aceDataArry[a].parentIdArry);
            }else{
              th.aceDataArry[a].notShow = true;
            }
          }
        }
        if(showNode && showNode.length > th.aceShowLength){
          for(let item of showNode){
            if(item.deep < (th.aceShowDeep - 1)){
              item.open = true;
            }else{
              item.open = false;
            }
          }
        }else{
          for(let item of showNode){
            item.open = true;
          }
        }
        if(showNode.length === 0){
          th.aceNodata = true;
        }else{
          th.aceNodata = false;
        }
      } else {
        for(let item of th.aceDataArry){
          item.notShow = false;
          if(item.deep < (th.aceShowDeep - 1)){
            item.open = true;
          }else{
            item.open = false;
          }
        }
        if(!th.aceDataArry || th.aceDataArry.length === 0){
          th.aceNodata = true;
        }else{
          th.aceNodata = false;
        }
      }
    }
  }

  //搜索框变化
  searchChange(value){
    let th = this;
    if(th.searchTimeout){
      clearTimeout(th.searchTimeout);
      th.searchTimeout = "";
    }
    th.searchTimeout = setTimeout(() => {
      th.aceQueryByName(value);
    }, 500);
  }

  aceSelectChanged(tree){
    let th = this;
    th.aceSelect = tree;
    th.aceSelectChange.emit(tree);
  }

  aceSetCheckAll(e){
    let th = this;
    if(th.aceDataArry && th.aceDataArry.length > 0){
      for(let item of th.aceDataArry){
        item.checked = e;
        item.halfChecked = false;
      }
    }
  }

  aceGetTreeNodeById(id){
    let th = this;
    if (th.aceDataArry && th.aceDataArry.length > 0) {
      for (let item of th.aceDataArry) {
        if (item.id == id) {
          return item;
        }
      }
    }
  }
  
  aceGetTreeNodeByKeyAndValue(key,value){
    let th = this;
    if (th.aceDataArry && th.aceDataArry.length > 0) {
      for (let item of th.aceDataArry) {
        if (item[key] == value) {
          return item;
        }
      }
    }
  }

  aceGetCheckedArry(){
    let th = this;
    let selectArry = [];
    if (th.aceDataArry && th.aceDataArry.length > 0) {
      for (let item of th.aceDataArry) {
        if(item.checked && !item.halfChecked){
          selectArry.push(item);
        }
      }
    }
    return selectArry;
  }

  aceGetCheckedAndHalfCheckedArry(){
    let th = this;
    let selectArry = [];
    if (th.aceDataArry && th.aceDataArry.length > 0) {
      for (let item of th.aceDataArry) {
        if(item.checked || item.halfChecked){
          selectArry.push(item);
        }
      }
    }
    return selectArry;
  }

  aceClearCheck() {
    let th = this;
    if (th.aceDataArry && th.aceDataArry.length > 0) {
      for (let item of th.aceDataArry) {
        item.checked = false;
        item.halfChecked = false;
      }
    }
  }

  //清除所有勾选状态，并且全部关闭，重置回初始化状态
  aceRetract(type:boolean = false) {
    let th = this;
    if (th.aceDataArry && th.aceDataArry.length > 0) {
      for (let item of th.aceDataArry) {
        item.checked = false;
        item.halfChecked = false;
        if(type){
          item.notShow = false;
        }
        if(item.deep < (th.aceShowDeep - 1)){
          item.open = true;
        }else{
          item.open = false;
        }
      }
    }
  }

  //当业务页面自己勾选后，更新所有子节点和父级节点的状态
  aceUpdateStatus() {
    let th = this;
    if(th.aceDataArry && th.aceDataArry.length > 0){
      for(var a = th.aceDataArry.length - 1;a >= 0;a --){
        if (th.aceDataArry[a].children && th.aceDataArry[a].children.length > 0) {
          th.updataTreeStatus(th.aceDataArry[a]);
        }
      }
    }
  }

  updataTreeStatus(treeNode) {
    if (treeNode.children.every(item => !item.checked && !item.halfChecked)) {
      treeNode.checked = false;
      treeNode.halfChecked = false;
    } else if (treeNode.children.every(item => item.checked && !item.halfChecked)) {
      treeNode.checked = true;
      treeNode.halfChecked = false;
    } else {
      treeNode.checked = true;
      treeNode.halfChecked = true;
    }
  }

  //作用：按键控制（有选中的继续向下调方法，没有选中内容则默认选中第一个）
  keyChange(keyCode){
    let th = this;
    let lastNode = th.getLastNode();
    let firstNode = th.getFirstNode();
    if(!lastNode){
      return;
    }
    if(th.aceSelect.id && !th.aceSelect.notShow){
      if(th.aceSelect.id && !th.aceFocusNode.id){
        th.aceFocusNode = JSON.stringify(JSON.parse(th.aceSelect));
        th.aceFocusNodeChange.emit(th.aceFocusNode);
        setTimeout(() => {
          th.getScorllTop(th.aceFocusNode);  
        }, 100);
      }
    }
    if(keyCode == 'down'){
      if(!th.aceFocusNode.id){
        th.aceFocusNode = firstNode;
        th.aceFocusNodeChange.emit(th.aceFocusNode);
        setTimeout(() => {
          th.getScorllTop(th.aceFocusNode);  
        }, 100);
      }else{
        if(th.aceFocusNode.id === lastNode.id){
          return;
        }else{
          th.keyChangeDetail(keyCode,th.aceFocusNode);
        }
      }
    }else if(keyCode == 'up'){
      if(th.aceFocusNode.id){
        if(firstNode.id != th.aceFocusNode.id){
          th.keyChangeDetail(keyCode,th.aceFocusNode);
        }
      }
    }
  }

  getLastNode(){
    let th = this;
    for(var a = (th.aceDataArry.length - 1);a >= 0;a --){
      if(!th.aceDataArry[a].notShow){
        return th.aceDataArry[a];
      }
    }
  }

  getFirstNode(){
    let th = this;
    for(var a = 0;a < th.aceDataArry.length;a ++){
      if(!th.aceDataArry[a].notShow){
        return th.aceDataArry[a];
      }
    }
  }

  getScorllTop(node){
    let th = this;
    let height = 0;
    let getNode = node;
    let index = 0;
    while(getNode.id){
      let id = th.aceUid + getNode.id;
      index ++;
      height = height + document.getElementById(id).offsetTop + 33;
      if(getNode.parentId){
        getNode = th.aceDateKey[getNode.parentId];
      }else{
        getNode = {};
      }
    }
    let scrollTop = document.getElementById(th.aceUid).scrollTop;
    if(height < (scrollTop + 40)){
      document.getElementById(th.aceUid).scrollTop = height - 40;
    }
    if(height > (310 + scrollTop)){
      document.getElementById(th.aceUid).scrollTop = height - 310;
    }
  }

  //作用：按键控制逻辑
  //入参：keyCode：按键“up”、“down”；select：选中节点；nextNodeMark:下级组件反馈上来的标志，主要是通过这个字段免去判断open
  keyChangeDetail(keyCode,select,nextNodeMark:any=false){
    let th =this;
    if(keyCode == 'down'){
      if(select.children && select.children.length > 0){
        let childrenFist:any = {};
        for(let item of select.children){
          if(!item.notShow && !childrenFist.id){
            childrenFist = item;
          }
        }
        if(childrenFist && childrenFist.id){
          th.aceFocusNode = childrenFist;
          select.open = true;
        }else{
          th.aceFocusNode = th.getParentNext(select);
        }
      }else{
        th.aceFocusNode = th.getParentNext(select);
      }
      th.setNodeParentOpen(th.aceFocusNode);
      setTimeout(() => {
        th.getScorllTop(th.aceFocusNode);  
      }, 100);

    }else if(keyCode == 'up'){
      th.aceFocusNode = th.getParentPrevious(select);
      th.setNodeParentOpen(th.aceFocusNode);
      setTimeout(() => {
        th.getScorllTop(th.aceFocusNode);  
      }, 100);
    }
    th.aceFocusNodeChange.emit(th.aceFocusNode);
  }

  getParentNext(node){
    let th = this;
    let nextNode = {};
    function getNext(node){
      if(node.parentId){
        let parent = th.aceDateKey[node.parentId];
        let childrenArry = [];
        let childrenIdArry = [];
        for(let item of parent.children){
          if(!item.notShow){
            childrenArry.push(item);
            childrenIdArry.push(item.id);
          }
        }
        let indexKey = childrenIdArry.indexOf(node.id);

        if(indexKey != (childrenArry.length - 1)){
          nextNode = childrenArry[indexKey + 1];
        }else{
          getNext(parent)
        }
      }else{
        let childrenArry = [];
        let childrenIdArry = [];
        for(let item of th.aceDataArry){
          if(!item.notShow){
            childrenArry.push(item);
            childrenIdArry.push(item.id);
          }
        }
        let indexKey = childrenIdArry.indexOf(node.id);
        if(indexKey != (childrenArry.length - 1)){
          nextNode = childrenArry[indexKey + 1];
        }
      }
      
    }
    getNext(node);
    return nextNode;
  }

  setNodeParentOpen(node){
    let th = this;
    if(node.parentId){
      let parentId = node.parentId;
      while(parentId){
        let parent = th.aceDateKey[parentId];
        parent.open = true;
        parentId = parent.parentId;
      }
    }
    
  }

  getParentPrevious(node){
    let th = this;
    if(node.parentId){
      let parent = th.aceDateKey[node.parentId];
      let childrenArry = [];
      let childrenIdArry = [];
      for(let item of parent.children){
        if(!item.notShow){
          childrenArry.push(item);
          childrenIdArry.push(item.id);
        }
      }
      let indexKey = childrenIdArry.indexOf(node.id);
      if(indexKey == 0){
        return parent;
      }else{
        return th.getChildrenLast(childrenArry[indexKey - 1]);
      }
    }else{
      let childrenArry = [];
      let childrenIdArry = [];
      for(let item of th.aceDataArry){
        if(!item.notShow){
          childrenArry.push(item);
          childrenIdArry.push(item.id);
        }
      }
      let indexKey = childrenIdArry.indexOf(node.id);
      if(indexKey == 0){
        return node;
      }else{
        return th.getChildrenLast(childrenArry[indexKey - 1]);
      }
    }
  }

  getChildrenLast(node){
    let previousNode = {};
    function getLastNode(node){
      for(var a = (node.children.length - 1);a >= 0;a --){
        if(!node.children[a].notShow){
          return node.children[a];
        }
      }
    }
    function getLast(node){
      if(node.children && node.children.length > 0){
        let lastNode = getLastNode(node);
        if(lastNode){
          node.open = true;
          getLast(lastNode);
        }else{
          previousNode = node;
        }
      }else{
        previousNode = node;
      }
    }
    getLast(node);
    return previousNode;
  }

  aceMultiSelectChange(){
    let th = this;
    th.aceSelectChange.emit();
  }

  ngOnChanges(changes:SimpleChange){
    let th = this;
    if(changes['aceTreeData']){
      th.setDataArry(this.aceTreeData);
    }
  }

}
