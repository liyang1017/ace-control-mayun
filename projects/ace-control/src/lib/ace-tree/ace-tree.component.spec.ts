import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { AceTreeComponent } from './ace-tree.component';

describe('AceTreeComponent', () => {
  let component: AceTreeComponent;
  let fixture: ComponentFixture<AceTreeComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ AceTreeComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(AceTreeComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
