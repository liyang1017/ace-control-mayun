import { Directive, ElementRef, Input, SimpleChange, HostListener } from '@angular/core';

@Directive({
  selector: '[ace-button]'
})
export class AceButtonDirective {

  @Input() aceType:String = "";
  @Input() aceSize:String = "m";
  @Input() aceGhost:Boolean = false;
  @Input() aceText:Boolean = false;
  @Input() aceIcon:String = "";
  @Input() aceBlock:Boolean = false;

  constructor(private el: ElementRef) {
  }

  ngAfterViewInit(){
    this.el.nativeElement.classList.add('ace-btn');
    this.aceSize = this.aceSize?this.aceSize:'m';
    if(this.aceGhost !== false){
      if(this.aceType == 'warn'){
        this.el.nativeElement.classList.add('ace-ghost-warn');
      }else{
        this.el.nativeElement.classList.add('ace-ghost-primary');
      }
      
      // this.el.nativeElement.style.bac
    }
    if(this.aceBlock !== false){
      this.el.nativeElement.style.width = '100%';
    }
    this.setSize();
    this.setType();
    if(this.aceIcon){
      this.setIcon();
    }
  }

  private sizeArry:Array<String> = ['xs','s','l','m'];
  private sizeClass:Array<String> = ['ace-button-very-small','ace-button-small','ace-button-large','ace-button-default'];

  private typeArry:Array<String> = ['primary','warn','link','text'];
  private typeClass:Array<String> = ['ace-primary-btn','ace-warn-btn','ace-link-btn','ace-text-btn'];


  setSize(old:String = ''){
    let th = this;
    if(old){
      let size = old.toLowerCase();
      let index = th.sizeArry.indexOf(size);
      th.el.nativeElement.classList.remove(index >= 0?th.sizeClass[index]:'default');
    }else{
      th.el.nativeElement.classList.remove('default');
    }
    let size = th.aceSize?th.aceSize.toLowerCase():'default';
    let index = th.sizeArry.indexOf(size);
    th.el.nativeElement.classList.add(index >= 0?th.sizeClass[index]:'default');
  }

  setType(old:String = ''){
    let th = this;
    if(old){
      let type = old.toLowerCase();
      let index = th.typeArry.indexOf(type);
      th.el.nativeElement.classList.remove(index >= 0?th.typeClass[index]:'ace-primary-btn');
    }else{
      th.el.nativeElement.classList.remove('ace-primary-btn');
    }
    let type = th.aceType?th.aceType.toLowerCase():'ace-primary-btn';
    let index = th.typeArry.indexOf(type);
    th.el.nativeElement.classList.add(index >= 0?th.typeClass[index]:'ace-primary-btn');
    if(th.aceType == 'text'){
      if(th.aceText !== false){
        th.el.nativeElement.classList.add('ace-text-important');
      }
    }
  }

  setIcon(){
    let th = this;
    let icon = document.createElement('i');
    icon.className = 'icon iconfontace ace-icon ' + th.aceIcon;
    th.el.nativeElement.insertBefore(icon,th.el.nativeElement.childNodes[0]);
  }

  ngOnChanges(changes:SimpleChange){
    let th = this;
    if(changes['aceSize']){
      th.setSize(changes['aceSize'].previousValue);
    }
    if(changes['aceType']){
      th.setType(changes['aceType'].previousValue);
    }
  }

  @HostListener('click',['$event.target'])
  onclick(btn){
    setTimeout(() => {
      btn.blur();
    }, 100);
  }
}
