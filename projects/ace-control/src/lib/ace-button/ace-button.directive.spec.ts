import { AceButtonDirective } from './ace-button.directive';

describe('AceButtonDirective', () => {
  it('should create an instance', () => {
    const el:any = '<button ace-button></button>'
    const directive = new AceButtonDirective(el);
    expect(directive).toBeTruthy();
  });
});
