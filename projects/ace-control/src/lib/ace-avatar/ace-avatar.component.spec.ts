import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { AceAvatarComponent } from './ace-avatar.component';

describe('AceAvatarComponent', () => {
  let component: AceAvatarComponent;
  let fixture: ComponentFixture<AceAvatarComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ AceAvatarComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(AceAvatarComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
