import {
  OnInit,
  Component,
  ElementRef,
  EventEmitter,
  Input,
  Output,
  ViewChild
} from "@angular/core";

@Component({
  selector: "ace-avatar",
  templateUrl: "./ace-avatar.component.html",
  styleUrls: ["./ace-avatar.component.less"]
})
export class AceAvatarComponent implements OnInit {
  @Input() aceShape: string = "circle";
  @Input() aceSize: string | Number = "m";
  @Input() aceIcon: string = "icon iconfontace aceUser";
  @Input() aceText: string;
  @Input() aceSrc: string;
  @Input() aceSrcSet: string;
  @Input() aceAlt: string;
  @Output() readonly aceError = new EventEmitter<Event>();

  @ViewChild("textEl", { static: false }) textEl: ElementRef;

  private el: HTMLElement = this.elementRef.nativeElement;
  private prefixCls = "ace-avatar";

  constructor(private elementRef: ElementRef) {}

  private setStyleClass(): void {
    this.el.classList.add(this.prefixCls);
    this.el.classList.add(this.prefixCls + "-" + this.aceShape);
    if (typeof this.aceSize === "number") {
      this.el.style.height = `${this.aceSize}px`;
      this.el.style.width = `${this.aceSize}px`;
    }
    else {
      this.el.classList.add(this.prefixCls + "-" + this.aceSize)
    }
    if (!!this.aceSrc) {
      this.el.classList.add(this.prefixCls + "-img");
    } else if (this.aceText) {
      this.el.classList.add(this.prefixCls + "-string");
    } else {
      this.el.classList.add(this.prefixCls + "-icon");
    }
  }

  ngAfterViewInit() {
    this.setStyleClass();
  }

  imgError($event: Event): void {
    this.aceError.emit($event);
    this.aceSrc = "";
  }

  ngOnInit() {}
}
