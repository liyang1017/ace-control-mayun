import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { HttpClientModule } from '@angular/common/http';
import { ReactiveFormsModule, FormsModule } from '@angular/forms';
import { AceControlService } from './ace-control.service';
import { AceAvatarComponent } from './ace-avatar/ace-avatar.component';
import { AceButtonDirective } from './ace-button/ace-button.directive';
import { AceButtonGroupComponent } from './ace-button-group/ace-button-group.component';
import { AceCheckboxComponent } from './ace-checkbox/ace-checkbox.component';
import { AceIconDirective } from './ace-icon/ace-icon.directive';
import { AceInputDirective } from './ace-input/ace-input.directive';
import { AceInputGroupComponent } from './ace-input-group/ace-input-group.component';
import { AceInputTreeComponent } from './ace-input-tree/ace-input-tree.component';
import { AceLoadingComponent } from './ace-loading/ace-loading.component';
import { AceNodataComponent } from './ace-nodata/ace-nodata.component';
import { AceTabComponent } from './ace-tab/ace-tab.component';
import { AceTabsComponent } from './ace-tabs/ace-tabs.component';
import { AceTagComponent } from './ace-tag/ace-tag.component';
import { AceTreeComponent } from './ace-tree/ace-tree.component';
import { AceTreeControlComponent } from './ace-tree/ace-tree-control/ace-tree-control.component';
<<<<<<< HEAD
import { AceBreadcrumbComponent } from './ace-breadcrumb/ace-breadcrumb.component';
import { AceBreadcrumbItemComponent } from './ace-breadcrumb/ace-breadcrumb-item/ace-breadcrumb-item.component';
@NgModule({
  imports: [
    CommonModule,
    HttpClientModule,
    FormsModule,
    ReactiveFormsModule
  ],
  declarations: [
      AceButtonDirective, AceButtonGroupComponent, AceCheckboxComponent, AceIconDirective, AceInputDirective, AceInputGroupComponent,
       AceInputTreeComponent, AceLoadingComponent,
       AceNodataComponent, AceTabComponent, AceTabsComponent, AceTagComponent, AceTreeComponent, AceTreeControlComponent,
       AceBreadcrumbComponent, AceBreadcrumbItemComponent],
  providers: [
    AceControlService
=======
import { AceRadioComponent } from './ace-radio/ace-radio.component';
import { AceBadgeComponent } from './ace-badge/ace-badge.component';
import { AceSwitchComponent } from './ace-switch/ace-switch.component';
import { AcePaginationComponent } from './ace-pagination/ace-pagination.component';
import { AceStepsComponent } from './ace-steps/ace-steps.component';
import { AceStepComponent } from './ace-steps/ace-step.component';
@NgModule({
  imports: [CommonModule, HttpClientModule, FormsModule, ReactiveFormsModule],
  declarations: [
    AceButtonDirective,
    AceButtonGroupComponent,
    AceCheckboxComponent,
    AceIconDirective,
    AceInputDirective,
    AceInputGroupComponent,
    AceInputTreeComponent,
    AceLoadingComponent,
    AceNodataComponent,
    AceTabComponent,
    AceTabsComponent,
    AceTagComponent,
    AceTreeComponent,
    AceTreeControlComponent,
    AceRadioComponent,
    AceSwitchComponent,
    AceBadgeComponent,
    AceAvatarComponent,
    AceStepsComponent,
    AceStepComponent,
    AcePaginationComponent
>>>>>>> 234ad865e2ecf2e0280f5a34988c19f24d0c418b
  ],
  providers: [AceControlService],
  exports: [
    AceButtonDirective,
<<<<<<< HEAD
    AceButtonGroupComponent, AceCheckboxComponent, AceIconDirective, AceInputDirective, AceInputGroupComponent, AceInputTreeComponent,
    AceLoadingComponent, AceNodataComponent, AceTabComponent, AceTabsComponent, AceTagComponent, AceTreeComponent, AceTreeControlComponent,
    AceBreadcrumbComponent, AceBreadcrumbItemComponent]
=======
    AceButtonGroupComponent,
    AceCheckboxComponent,
    AceIconDirective,
    AceInputDirective,
    AceInputGroupComponent,
    AceInputTreeComponent,
    AceLoadingComponent,
    AceNodataComponent,
    AceTabComponent,
    AceTabsComponent,
    AceTagComponent,
    AceTreeComponent,
    AceTreeControlComponent,
    AceRadioComponent,
    AceSwitchComponent,
    AceBadgeComponent,
    AceAvatarComponent,
    AceStepsComponent,
    AceStepComponent,
    AcePaginationComponent
  ]
>>>>>>> 234ad865e2ecf2e0280f5a34988c19f24d0c418b
})
export class AceControlModule {}
