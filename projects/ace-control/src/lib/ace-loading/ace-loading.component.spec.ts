import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { AceLoadingComponent } from './ace-loading.component';

describe('AceLoadingComponent', () => {
  let component: AceLoadingComponent;
  let fixture: ComponentFixture<AceLoadingComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ AceLoadingComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(AceLoadingComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
