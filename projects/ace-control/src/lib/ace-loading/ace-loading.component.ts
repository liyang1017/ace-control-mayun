import { Component, OnInit, Input,TemplateRef } from '@angular/core';

@Component({
  selector: 'ace-loading',
  templateUrl: './ace-loading.component.html',
  styleUrls: ['./ace-loading.component.less']
})
export class AceLoadingComponent implements OnInit {

  constructor() { }

  @Input() aceLoad:Boolean = false;
  @Input() aceText:String = '正在加载';
  @Input() aceTemplate:TemplateRef<any>;

  ngOnInit() {
  }


}
