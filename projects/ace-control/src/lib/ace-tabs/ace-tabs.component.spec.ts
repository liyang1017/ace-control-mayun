import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { AceTabsComponent } from './ace-tabs.component';

describe('AceTabsComponent', () => {
  let component: AceTabsComponent;
  let fixture: ComponentFixture<AceTabsComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ AceTabsComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(AceTabsComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
