import { Component, OnInit, Input, ContentChildren, QueryList, Output, EventEmitter, SimpleChanges } from '@angular/core';
import { AceTabComponent } from './../ace-tab/ace-tab.component';

@Component({
  selector: 'ace-tabs',
  templateUrl: './ace-tabs.component.html',
  styleUrls: ['./ace-tabs.component.less']
})
export class AceTabsComponent implements OnInit {

  constructor() { }

  @ContentChildren(AceTabComponent) private tabArry: QueryList<AceTabComponent>;

  @Input() aceType: String = "default";

  tabTypeClass: String = "ace-tabs-default";

  tabWidth: String = 'auto';

  @Input() aceSelectKey: Number | String = 0;

  @Output() aceSelectKeyChange = new EventEmitter();
  @Output() aceSelectChange = new EventEmitter();

  ngOnInit() {
    this.setType();
  }

  ngAfterContentInit() {
    let th = this;
    console.log(th.tabArry);
    if (th.tabArry['_results'] && th.tabArry['_results'].length > 0) {
      if(th.aceType == 'default'){
        th.tabWidth = (100 / th.tabArry['_results'].length) + '%';
      }
      for (let a = 0; a < th.tabArry['_results'].length; a++) {
        let item = th.tabArry['_results'][a];
        item.tabWidth = th.tabWidth;
        item.aceLength = th.tabArry['_results'].length;
        item.key = a;
        item.selectKey = th.aceSelectKey;
        item.aceSelectKeyChange.subscribe((data: any) => {
          th.selectKeyChange(data);
        })
      }
    }
  }

  selectKeyChange(key) {
    let th = this;
    let classTab = 'slideInLeft';
    if (key < th.aceSelectKey) {
      classTab = 'slideInRight';
    }
    for (let a = 0; a < th.tabArry['_results'].length; a++) {
      let item = th.tabArry['_results'][a];
      item.classTab = classTab;
      item.aceSelectKey = key;
    }
    th.aceSelectKey = key;
    th.aceSelectKeyChange.emit(th.aceSelectKey);
    th.aceSelectChange.emit(th.aceSelectKey);
  }

  setType() {
    let th = this;
    if (th.aceType == 'default') {
      th.tabTypeClass = 'ace-tabs-default';
    } else if (th.aceType == 'primary') {
      th.tabTypeClass = 'ace-tabs-primary';
    } else if (th.aceType == 'capsule') {
      th.tabTypeClass = 'ace-tabs-capsule';
    }
  }

  ngOnChanges(changes: SimpleChanges) {
    let th = this;
    if (changes.aceSelectKey) {
      if (th.tabArry && th.tabArry['_results'] && th.tabArry['_results'].length > 0) {
        if (changes.aceSelectKey.currentValue > changes.aceSelectKey.previousValue) {
          let classTab = 'slideInLeft';
          for (let a = 0; a < th.tabArry['_results'].length; a++) {
            let item = th.tabArry['_results'][a];
            item.classTab = classTab;
            item.aceSelectKey = changes.aceSelectKey.currentValue;
          }
        } else {
          let classTab = 'slideInRight';
          for (let a = 0; a < th.tabArry['_results'].length; a++) {
            let item = th.tabArry['_results'][a];
            item.classTab = classTab;
            item.aceSelectKey = changes.aceSelectKey.currentValue;
          }
        }
      }
    }
  }
}
