import {
  Component,
  OnInit,
  ElementRef,
  EventEmitter,
  Input,
  Output,
  ViewChild,
  Renderer2,
  TemplateRef
} from "@angular/core";

@Component({
  selector: "ace-step",
  templateUrl: "./ace-step.component.html",
  exportAs: "aceStep",
  host: {
    "[class.ace-step-wait]": 'aceStatus === "wait"',
    "[class.ace-step-process]": 'aceStatus === "process"',
    "[class.ace-step-finish]": 'aceStatus === "finish"',
    "[class.ace-step-error]": 'aceStatus === "error"',
    "[class.ace-step-active]": "currentIndex === index",
    "[class.ace-step-disabled]": "aceDisabled"
  }
})
export class AceStepComponent implements OnInit {
  @ViewChild("aceTemplate", { static: false }) aceTemplate: TemplateRef<void>;

  ngOnInit() {}
  title;
  titleIsTemplate = false;
  @Input()
  get aceTitle(): string | TemplateRef<void> {
    return this.title;
  }
  set aceTitle(value: string | TemplateRef<void>) {
    this.title = value;
    if (value instanceof TemplateRef) {
      this.titleIsTemplate = true;
    } else this.titleIsTemplate = false;
  }
  description;
  descriptionIsTemplate = false;
  @Input()
  get aceDescription(): string | TemplateRef<void> {
    return this.description;
  }
  set aceDescription(value: string | TemplateRef<void>) {
    this.description = value;
    if (value instanceof TemplateRef) {
      this.descriptionIsTemplate = true;
    } else this.descriptionIsTemplate = false;
  }
  // @Input() aceDescription: string | TemplateRef<void>;
  @Input() aceDisabled: boolean = false;
  @Input() aceIcon: string = "";
  @Output() readonly aceIndexChange = new EventEmitter();

  statusClassMap = {
    finish: "aceCheck-Circle",
    error: "aceClose-Circle",
    process: "process",
    wait: "wait"
  };
  @Input()
  get aceStatus(): string {
    return this._status;
  }
  set aceStatus(status: string) {
    this._status = status;
    this.isCustomStatus = true;
    this.aceIcon = "icon iconfontace " + this.statusClassMap[this._status];
  }

  customProcessTemplate: TemplateRef<{
    $implicit: TemplateRef<void>;
    status: string;
    index: number;
  }>; // Set by parent.

  outStatus = "process";
  showProcessDot = false;
  direction = "horizontal";
  index = 0;
  last = false;

  private isCustomStatus = false;
  private _status = "wait";
  private _currentIndex = 0;

  get currentIndex(): number {
    return this._currentIndex;
  }

  set currentIndex(current: number) {
    this._currentIndex = current;
    if (!this.isCustomStatus) {
      this._status =
        current > this.index
          ? "finish"
          : current === this.index
          ? this.outStatus || ""
          : "wait";
      this.aceIcon = "icon iconfontace " + this.statusClassMap[this._status];
    }
  }

  constructor(renderer: Renderer2, elementRef: ElementRef) {
    renderer.addClass(elementRef.nativeElement, "ace-step");
  }

  onClick(): void {
    if (this.currentIndex !== this.index && !this.aceDisabled) {
      this.aceIndexChange.emit(this.index);
    }
  }
}
