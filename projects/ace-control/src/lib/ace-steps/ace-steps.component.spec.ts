import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { AceStepsComponent } from './ace-steps.component';

describe('AceStepsComponent', () => {
  let component: AceStepsComponent;
  let fixture: ComponentFixture<AceStepsComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ AceStepsComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(AceStepsComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
