import {
  Component,
  ContentChildren,
  Input,
  Output,
  EventEmitter,
  OnInit,
  TemplateRef,
  QueryList,
  SimpleChanges
} from "@angular/core";
import { AceStepComponent } from "./ace-step.component";
type AceStatusType = "wait" | "process" | "finish" | "error";

@Component({
  selector: "ace-steps",
  templateUrl: "./ace-steps.component.html"
})
export class AceStepsComponent implements OnInit {
  constructor() {}
  @ContentChildren(AceStepComponent) steps: QueryList<AceStepComponent>;
  @Input() aceCurrent: number = 0;
  @Input() aceDirection: string = "horizonal";
  @Input() aceSize: string = "l";
  @Input() aceStatus: AceStatusType = "process";
  @Output() readonly aceIndexChange = new EventEmitter<number>();

  @Input()
  set aceProgressDot(
    value:
      | boolean
      | TemplateRef<{
          $implicit: TemplateRef<void>;
          status: string;
          index: number;
        }>
  ) {
    if (value instanceof TemplateRef) {
      this.showProcessDot = true;
      this.customProcessDotTemplate = value;
    } else {
      this.showProcessDot = !!value;
    }
    this.updateChildrenSteps();
  }
  @Output() readonly nzIndexChange = new EventEmitter<number>();

  showProcessDot = false;
  customProcessDotTemplate: TemplateRef<{
    $implicit: TemplateRef<void>;
    status: string;
    index: number;
  }>;

  classMap: object;

  ngOnChanges(changes: SimpleChanges): void {
    if (changes.aceStatus || changes.aceCurrent) {
      this.updateChildrenSteps();
    }
    if (changes.aceDirection || changes.aceProgressDot || changes.aceSize) {
      this.setClassMap();
    }
  }

  ngOnInit(): void {
    this.setClassMap();
    this.updateChildrenSteps();
  }

  ngAfterContentInit(): void {
    this.updateChildrenSteps();
  }

  private updateChildrenSteps(): void {
    if (this.steps) {
      const length = this.steps.length;
      this.steps.toArray().forEach((step, index) => {
        Promise.resolve().then(() => {
          step.outStatus = this.aceStatus;
          step.showProcessDot = this.showProcessDot;
          if (this.customProcessDotTemplate) {
            step.customProcessTemplate = this.customProcessDotTemplate;
          }
          step.direction = this.aceDirection;
          step.index = index;
          step.last = length === index + 1;
          step.currentIndex = this.aceCurrent;
          step.aceIndexChange.subscribe(() => {
            console.log(index, "aceIndexChange");
            this.aceIndexChange.emit(index);
          });
        });
      });
    }
  }

  private setClassMap(): void {
    this.classMap = {
      [`ace-steps-${this.aceDirection}`]: true,
      [`ace-steps-dot-${this.showProcessDot}`]: true,
      [`ace-steps-${this.aceSize}`]: true
    };
  }
}
