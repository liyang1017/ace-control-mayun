import { Component, OnInit,ElementRef,ContentChildren,QueryList } from '@angular/core';
import { AceButtonDirective } from './../ace-button/ace-button.directive';

@Component({
  selector: 'ace-button-group',
  templateUrl: './ace-button-group.component.html',
  styleUrls: ['./ace-button-group.component.less']
})
export class AceButtonGroupComponent implements OnInit {

  constructor(
    private el:ElementRef
  ) { 
  }

  @ContentChildren(AceButtonDirective) private buttonGroup:QueryList<AceButtonDirective>;

  ngOnInit() {
  }

  ngAfterContentInit(){
    let th = this;
    if(th.buttonGroup['_results'] && th.buttonGroup['_results'].length > 0){
      for(let a = 0;a < th.buttonGroup['_results'].length;a ++){
        let item = th.buttonGroup['_results'][a];
        if(item.aceGhost !== false){
          if(a == 0){
            item.el.nativeElement.classList.add('ace-btn-group-ghost-left');
          }else if(a == (th.buttonGroup['_results'].length - 1)){
            item.el.nativeElement.classList.add('ace-btn-group-ghost-right');
          }else{
            item.el.nativeElement.classList.add('ace-btn-group-ghost-middle');
          }
        }else{
          if(a == 0){
            item.el.nativeElement.classList.add('ace-btn-group-left');
          }else if(a == (th.buttonGroup['_results'].length - 1)){
            item.el.nativeElement.classList.add('ace-btn-group-right');
          }else{
            item.el.nativeElement.classList.add('ace-btn-group-middle');
          }
        }
      }
    }
  }

}
