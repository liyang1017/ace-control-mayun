import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { AceButtonGroupComponent } from './ace-button-group.component';

describe('AceButtonGroupComponent', () => {
  let component: AceButtonGroupComponent;
  let fixture: ComponentFixture<AceButtonGroupComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ AceButtonGroupComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(AceButtonGroupComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
