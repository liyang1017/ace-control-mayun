import { Component, OnInit, Input, Output, EventEmitter } from '@angular/core';

@Component({
    selector: 'ace-radio',
    templateUrl: './ace-radio.component.html',
    styleUrls: ['./ace-radio.component.less']
})
export class AceRadioComponent implements OnInit {

    constructor() { }

    @Input() aceModel: boolean = false;
    @Input() disabled: boolean = false;

    @Output() aceModelChange = new EventEmitter();

    radioClass = 'fadeIn';

    ngOnInit() {
        console.log(this.aceModel)
    }

    changeSelect() {
        let th = this;
        if (th.disabled) return;
        th.aceModel = !th.aceModel;
        th.aceModelChange.emit(th.aceModel);
    }

}
