import { TestBed } from '@angular/core/testing';

import { AceControlService } from './ace-control.service';

describe('AceControlService', () => {
  beforeEach(() => TestBed.configureTestingModule({}));

  it('should be created', () => {
    const service: AceControlService = TestBed.get(AceControlService);
    expect(service).toBeTruthy();
  });
});
