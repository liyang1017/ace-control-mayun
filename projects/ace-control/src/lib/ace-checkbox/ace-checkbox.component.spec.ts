import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { AceCheckboxComponent } from './ace-checkbox.component';

describe('AceCheckboxComponent', () => {
  let component: AceCheckboxComponent;
  let fixture: ComponentFixture<AceCheckboxComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ AceCheckboxComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(AceCheckboxComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
