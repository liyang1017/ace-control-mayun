import { Component, OnInit, Input, Output, EventEmitter } from '@angular/core';

@Component({
  selector: 'ace-checkbox',
  templateUrl: './ace-checkbox.component.html',
  styleUrls: ['./ace-checkbox.component.less']
})
export class AceCheckboxComponent implements OnInit {

  constructor() { }

  @Input() aceModel:boolean = false;
  @Input() disabled:boolean = false;
  @Input() aceIndeterminate:boolean = false;

  @Output() aceModelChange = new EventEmitter();
  @Output() aceIndeterminateChange = new EventEmitter();

  checkboxClass = 'fadeIn';


  ngOnInit() {
    console.log(this.aceModel)
  }

  changeSelect(){
    let th = this;
    if(th.disabled)return;
    if(th.aceIndeterminate){
      th.aceIndeterminate = false;
      th.aceModel = true;
    }else{
      th.aceModel = !th.aceModel;
    }

    th.aceModelChange.emit(th.aceModel);
    th.aceIndeterminateChange.emit(th.aceIndeterminate);
    
  }

}
