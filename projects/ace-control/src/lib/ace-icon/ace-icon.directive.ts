import { Directive, ElementRef, Input } from '@angular/core';

@Directive({
  selector: '[ace-icon]'
})
export class AceIconDirective {

  constructor(private el:ElementRef) { }

  @Input() icon:String;

  ngAfterViewInit(){
    this.el.nativeElement.classList.add('ace-icon');
    if(this.icon){
      this.el.nativeElement.classList.add('icon');
      this.el.nativeElement.classList.add('iconfontace');
      this.el.nativeElement.classList.add(this.icon);
    }
  }

}
