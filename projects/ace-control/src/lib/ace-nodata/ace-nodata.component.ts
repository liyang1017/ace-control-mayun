import { Component, OnInit, Input, TemplateRef } from '@angular/core';


@Component({
  selector: 'ace-nodata',
  templateUrl: './ace-nodata.component.html',
  styleUrls: ['./ace-nodata.component.less']
})
export class AceNodataComponent implements OnInit {

  constructor() { }

  @Input() aceNodata:Boolean = false;
  @Input() aceText:String = '暂无数据';
  @Input() aceTemplate:TemplateRef<any>;

  ngOnInit() {
  }


}
