import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { AceNodataComponent } from './ace-nodata.component';

describe('AceNodataComponent', () => {
  let component: AceNodataComponent;
  let fixture: ComponentFixture<AceNodataComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ AceNodataComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(AceNodataComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
