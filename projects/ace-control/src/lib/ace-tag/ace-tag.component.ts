import { Component, OnInit, Input, Output, EventEmitter } from '@angular/core';
import { AceControlService } from '../ace-control.service';

@Component({
  selector: 'ace-tag',
  templateUrl: './ace-tag.component.html',
  styleUrls: ['./ace-tag.component.less']
})
export class AceTagComponent implements OnInit {

  constructor(
    private aceService: AceControlService
  ) { }

  @Input() aceTitle: String = "标签";
  @Input() aceType: String = "default";
  @Input() aceGhost: Boolean = false;
  @Input() aceCustomColor: String = "";
  @Input() aceShowClose: Boolean = false;
  @Input() aceShow: Boolean = true;
  @Input() aceSelect: Boolean = false;
  @Input() aceIsSelected: Boolean = false;
  @Output() aceClosed = new EventEmitter();
  @Input() aceOnClose:Function;
  @Output() aceShowChange = new EventEmitter();
  @Output() aceIsSelectedChange = new EventEmitter();
  tagClass: String = "ace-default-tag";
  backgroundColor = '';
  tagStyle: any = {};

  ngOnInit() {
    let th = this;
    if(th.aceSelect !== false){
      th.setSelect();
      return;
    }
    if (!th.aceCustomColor) {
      if (th.aceType == 'default') {
        th.tagClass = 'ace-default-tag'
      } else if (th.aceType == 'primary') {
        th.tagClass = 'ace-tag ace-primary-tag'
      }
    } else {
      th.tagClass = 'ace-tag';
      if (th.aceGhost !== false) {
        let bacColor = "";
        if (th.aceCustomColor.indexOf('rgb') > -1) {
          bacColor = th.aceService.getHexadecimalByRgba(th.aceCustomColor);
          bacColor = th.aceService.getRgbaByHexadecimal(bacColor, 0.1);
        } else {
          bacColor = th.aceService.getRgbaByHexadecimal(th.aceCustomColor, 0.1);
        }
        th.tagStyle = {
          "color": th.aceCustomColor,
          "border": '1px solid ' + th.aceCustomColor,
          "background-color": bacColor
        };
      } else {
        th.tagStyle = {
          "color": '#fff',
          "border": '1px solid ' + th.aceCustomColor,
          "background-color": th.aceCustomColor
        };
      }
    }
    if (th.aceGhost !== false) {
      th.tagClass = th.tagClass + ' ace-ghost-tag';
    }
  }

  //关闭标签的方法
  close() {
    let th = this;
    if(!th.aceOnClose){
      th.aceShow = false;
      th.aceShowChange.emit(th.aceShow);
      th.aceClosed.emit(th.aceShow);
    }else{
      let callBackData = th.aceOnClose();
      if(callBackData){
        th.aceShow = false;
        th.aceShowChange.emit(th.aceShow);
        th.aceClosed.emit(th.aceShow);
      }
    }  
  }

  //设置选择模式
  setSelect(){
    let th = this;
    th.tagClass = 'ace-select-tag';
    if(th.aceCustomColor){
      th.tagClass = 'ace-select-tag';
    }else{
      th.tagClass = 'ace-select-tag ace-select-hover';
    }
    th.selectChangeFun();
  }

  selectFun(Initialization = false){
    let th = this;
    if(th.aceSelect === false)return;
    if(!Initialization){
      th.aceIsSelected = !th.aceIsSelected;
      th.aceIsSelectedChange.emit(th.aceIsSelected);
    }
    if(th.aceIsSelected){
      if(th.aceCustomColor){
        let bacColor = "";
        if (th.aceCustomColor.indexOf('rgb') > -1) {
          bacColor = th.aceService.getHexadecimalByRgba(th.aceCustomColor);
          bacColor = th.aceService.getRgbaByHexadecimal(bacColor, 0.8);
        } else {
          bacColor = th.aceService.getRgbaByHexadecimal(th.aceCustomColor, 0.8);
        }
        th.tagStyle = {
          "color": '#fff',
          "border": '1px solid ' + bacColor,
          "background-color": bacColor
        };
      }else{
        th.tagClass = th.tagClass + ' ace-select-selected';
      }
    }else{
      if(th.aceCustomColor){
        th.tagStyle = {
          color:th.aceCustomColor
        };
      }else{
        th.tagClass = 'ace-select-tag ace-select-hover';
      }
    }
  }

  selectChangeFun(){
    let th = this;
    if(th.aceSelect === false)return;
    if(th.aceIsSelected){
      if(th.aceCustomColor){
        th.tagStyle = {
          "color": '#fff',
          "border": '1px solid ' + th.aceCustomColor,
          "background-color": th.aceCustomColor
        };
      }else{
        th.tagClass = th.tagClass + ' ace-select-selected';
      }
    }else{
      if(th.aceCustomColor){
        th.tagStyle = {};
      }else{
        th.tagClass = 'ace-select-tag ace-select-hover';
      }
    }
  }

  //鼠标移入
  mouseover(){
    let th = this;
    if(!th.aceCustomColor || th.aceSelect === false)return;
    if(th.aceIsSelected){
      let bacColor = "";
      if (th.aceCustomColor.indexOf('rgb') > -1) {
        bacColor = th.aceService.getHexadecimalByRgba(th.aceCustomColor);
        bacColor = th.aceService.getRgbaByHexadecimal(bacColor, 0.8);
      } else {
        bacColor = th.aceService.getRgbaByHexadecimal(th.aceCustomColor, 0.8);
      }
      th.tagStyle.border = '1px solid ' + bacColor;
      th.tagStyle['background-color'] = bacColor;
    }else{
      th.tagStyle.color = th.aceCustomColor;
    }
  }

  //鼠标移出
  mouseout(){
    let th = this;
    if(!th.aceCustomColor || th.aceSelect === false)return;
    if(th.aceIsSelected){
      th.tagStyle = {
        "color": '#fff',
        "border": '1px solid ' + th.aceCustomColor,
        "background-color": th.aceCustomColor
      };
    }else{
      th.tagStyle = {};
    }
  }

}
