import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { AceTagComponent } from './ace-tag.component';

describe('AceTagComponent', () => {
  let component: AceTagComponent;
  let fixture: ComponentFixture<AceTagComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ AceTagComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(AceTagComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
