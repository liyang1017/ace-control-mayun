import { Injectable } from '@angular/core';

@Injectable()
export class AceControlService {

  constructor() { }

  public getRgbaByHexadecimal = (sHex, alpha = 1) => {
    var reg = /^#([0-9a-fA-f]{3}|[0-9a-fA-f]{6})$/
    /* 16进制颜色转为RGB格式 */
    let sColor = sHex.toLowerCase()
    if (sColor && reg.test(sColor)) {
      if (sColor.length === 4) {
        var sColorNew = '#'
        for (let i = 1; i < 4; i += 1) {
          sColorNew += sColor.slice(i, i + 1).concat(sColor.slice(i, i + 1))
        }
        sColor = sColorNew
      }
      //  处理六位的颜色值
      var sColorChange = []
      for (let i = 1; i < 7; i += 2) {
        sColorChange.push(parseInt('0x' + sColor.slice(i, i + 2)))
      }
      // return sColorChange.join(',')
      // 或
      return 'rgba(' + sColorChange.join(',') + ',' + alpha + ')'
    } else {
      return sColor

    }
  }

  public getHexadecimalByRgba(value) {
    if (value.indexOf('rgba') > -1) {
      var values = value
        .replace(/rgba?\(/, '')
        .replace(/\)/, '')
        .replace(/[\s+]/g, '')
        .split(',');
      var a = parseFloat(values[3] || 1),
        r = Math.floor(a * parseInt(values[0]) + (1 - a) * 255),
        g = Math.floor(a * parseInt(values[1]) + (1 - a) * 255),
        b = Math.floor(a * parseInt(values[2]) + (1 - a) * 255);
        
        // a = Math.floor(a * parseInt(values[3]) + (1 - a) * 255);
      return "#" +
        ("0" + r.toString(16)).slice(-2) +
        ("0" + g.toString(16)).slice(-2) +
        ("0" + b.toString(16)).slice(-2);
    } else if (value.indexOf('rgb') > -1) {
      var array = value.split(",");
      if (array.length > 3)
        return "";
      value = "#";
      for (var i = 0, color; color = array[i++];) {
        color = parseInt(color.replace(/[^\d]/gi, ''), 10).toString(16);
        value += color.length == 1 ? "0" + color : color;
      }
      value = value.toUpperCase();
      return value;
    }


  }
}
