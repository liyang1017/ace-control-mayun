import { AceInputDirective } from './ace-input.directive';

describe('AceInputDirective', () => {
  it('should create an instance', () => {
    const el:any = '<button ace-button></button>'
    const directive = new AceInputDirective(el);
    expect(directive).toBeTruthy();
  });
});
