import { Directive,ElementRef, Input, HostListener, SimpleChange } from '@angular/core';

@Directive({
  selector: '[ace-input]'
})
export class AceInputDirective {

  constructor(
    private el:ElementRef
  ) { }

  @Input() aceSize:String = "m";
  @Input() aceBlock:Boolean = false;
  @Input() aceNoResize:Boolean = false;

  @Input() aceHover:boolean = false;

  private sizeArry:Array<String> = ['xs','s','l','m'];
  private sizeClass:Array<String> = ['ace-input-very-small','ace-input-small','ace-input-large','ace-input-default'];

  ngAfterViewInit(){
    this.aceSize = this.aceSize?this.aceSize:'m';
    if(this.el.nativeElement.type != 'textarea'){
      this.setSize();
    }
    if(this.el.nativeElement.type === 'text'){
      this.el.nativeElement.classList.add('ace-input');
    } else if(this.el.nativeElement.type === 'textarea'){
      this.el.nativeElement.classList.add('ace-textarea');
      if(this.aceNoResize !== false){
        this.el.nativeElement.style.resize = 'none';
      }
    }
    if(this.aceBlock !== false){
      this.el.nativeElement.style.width = '100%';
    }
  }

  setSize(old:String = ''){
    let th = this;
    if(old){
      let size = old.toLowerCase();
      let index = th.sizeArry.indexOf(size);
      th.el.nativeElement.classList.remove(index >= 0?th.sizeClass[index]:'ace-input-default');
    }else{
      th.el.nativeElement.classList.remove('ace-input-default');
    }
    let size = th.aceSize?th.aceSize.toLowerCase():'ace-input-default';
    let index = th.sizeArry.indexOf(size);
    th.el.nativeElement.classList.add(index >= 0?th.sizeClass[index]:'ace-input-default');
  }

  @HostListener('mouseover',['$event.target'])
  addHover(btn){
    this.aceHover = true;
    this.setInputHover();
  }
  @HostListener('mouseout',['$event.target'])
  removeHover(btn){
    this.aceHover = false;
    this.setInputHover();
  }

  private setInputHover(){
    if(this.aceHover){
      this.el.nativeElement.classList.add('ace-input-hover');
    }else{
      this.el.nativeElement.classList.remove('ace-input-hover');
    }
  }
}
