import {
  ChangeDetectionStrategy,
  ChangeDetectorRef,
  Component,
  ElementRef,
  Injector,
  Input,
  NgZone,
  OnDestroy,
  OnInit,
  Renderer2,
  TemplateRef,
  ViewEncapsulation
} from '@angular/core';
import { ActivatedRoute, NavigationEnd, Params, PRIMARY_OUTLET, Router } from '@angular/router';
import { Subject } from 'rxjs';
import { filter, startWith, takeUntil } from 'rxjs/operators'; 

interface BreadcrumbOption {
  label: string;
  params: Params;
  url: string;
}

@Component({
  // tslint:disable-next-line:component-selector
  selector: 'ace-breadcrumb',
  exportAs: 'aceBreadcrumb',
  preserveWhitespaces: false,
  templateUrl: './ace-breadcrumb.component.html',
  styleUrls: ['./ace-breadcrumb.component.less']
})
export class AceBreadcrumbComponent implements OnInit, OnDestroy {
  // @Input() aceIcon: String = '';
  // @Input() breadcrumbList: any;
  // @Input() separator: String= '/';
  // @Input() aceTemplate: TemplateRef<any>;
  // @Output() aceSelectKeyChange = new EventEmitter();
  // @ViewChild('cardsList', { static: true }) cardaList: ElementRef<HTMLDivElement>;

  @Input() aceAutoGenerate = false;
  @Input() aceSeparator: string | TemplateRef<void> = '/'; // 分隔符
  @Input() aceRouteLabel = 'breadcrumb';

  breadcrumbs: BreadcrumbOption[] | undefined = [];

  private destroy$ = new Subject<void>();

  constructor(
    private injector: Injector,
    private ngZone: NgZone,
    private cdr: ChangeDetectorRef,
    elementRef: ElementRef,
    renderer: Renderer2
  ) {
    renderer.addClass(elementRef.nativeElement, 'ace-breadcrumb');
    const aceIocn = document.getElementsByClassName('ace-icon');
  }
  ngOnInit(): void {
    if (this.aceAutoGenerate) {
      this.routerChange();
    }
  }

  ngOnDestroy(): void {
    this.destroy$.next();
    this.destroy$.complete();
  }

  navigate(url: string, e: MouseEvent): void {
    e.preventDefault();
    console.log(e);
    this.ngZone
      .run(() =>
        this.injector
          .get(Router)
          .navigateByUrl(url)
          .then()
      )
      .then();
  }

  private routerChange(): void {
    try {
      const router = this.injector.get(Router);
      const activatedRoute = this.injector.get(ActivatedRoute);
      router.events
        .pipe(
          filter(e => e instanceof NavigationEnd),
          takeUntil(this.destroy$),
          startWith(true)
        )
        .subscribe(() => {
          this.breadcrumbs = this.getBreadcrumbs(activatedRoute.root);
          this.cdr.markForCheck();
        });
    } catch (e) {
      throw new Error(`You should import RouterModule if you want to use 'aceAutoGenerate'.`);
    }
  }

  private getBreadcrumbs(route: ActivatedRoute, url: string = '', breadcrumbs: BreadcrumbOption[] = []): BreadcrumbOption[] | undefined {
    const children: ActivatedRoute[] = route.children;
     
    if (children.length === 0) {
      return breadcrumbs;
    }
    for (const child of children) {
      if (child.outlet === PRIMARY_OUTLET) {

        const routeURL: string = child.snapshot.url.map(segment => segment.path).join('/');
        const nextUrl = url + `/${routeURL}`;
        const breadcrumbLabel = child.snapshot.data[this.aceRouteLabel];

        if (routeURL && breadcrumbLabel) {
          const breadcrumb: BreadcrumbOption = {
            label: breadcrumbLabel,
            params: child.snapshot.params,
            url: nextUrl
          };
          breadcrumbs.push(breadcrumb);
        }
        console.log(breadcrumbs);
        return this.getBreadcrumbs(child, nextUrl, breadcrumbs);
      }
    }
    return undefined;
  }
}
