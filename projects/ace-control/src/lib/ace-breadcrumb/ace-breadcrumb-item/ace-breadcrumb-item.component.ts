import { AceBreadcrumbComponent } from './../ace-breadcrumb.component';
import { Component, OnInit } from '@angular/core';

@Component({
  // tslint:disable-next-line:component-selector
  selector: 'ace-breadcrumb-item',
  preserveWhitespaces: false,
  templateUrl: './ace-breadcrumb-item.component.html',
  styleUrls: ['./ace-breadcrumb-item.component.less']
})
export class AceBreadcrumbItemComponent implements OnInit {

  constructor(public aceBreadcrumbComponent: AceBreadcrumbComponent) { }

  ngOnInit() {
  }

}
