const fs = require('fs');
fs.copyFile('./src/ace.less', './dist/ace-control/ace.less', err => {
    console.log('ace.less文件复制移动到dist下成功');
})

const copyPath = './src/less';
const movePath = './dist/ace-control/less';
function copyFolder(copyPath,movePath,mkdirName){
    if(!fs.existsSync(movePath)){
        fs.mkdirSync(movePath);
    }
    const files = fs.readdirSync(copyPath, { withFileTypes: true });
    for (let i = 0; i < files.length; i++){
        const cf = files[i];
        if (cf.isFile()){
            fs.copyFile(copyPath + '/' + cf.name, movePath + '/' + cf.name, err => {
                console.log(cf.name +'文件复制移动到' + movePath + '下成功');
            })
        }else{
            copyFolder(copyPath + '/' + cf.name,movePath + '/' + cf.name)
        }
    }

}
copyFolder(copyPath,movePath);